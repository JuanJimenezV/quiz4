<?php

use Illuminate\Database\Seeder;

class seed_authors_table extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('authors')->insert([
            'name'=>'Michael Connelly',
            'country'=>'United States'
        ]);
        DB::table('authors')->insert([
            'name'=>'Isaac Asimov',
            'country'=>'United States'
        ]);
        DB::table('authors')->insert([
            'name'=>'Stephen King',
            'country'=>'United States'
        ]);
        DB::table('authors')->insert([
            'name'=>'Don DeLillo',
            'country'=>'United States'
        ]);
        DB::table('authors')->insert([
            'name'=>'Walter Mosely',
            'country'=>'United States'
        ]);
    }
}

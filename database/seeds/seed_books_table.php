<?php

use Illuminate\Database\Seeder;

class seed_books_table extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books')->insert([
            'title'=>'Black Echo',
            'num_pages'=>'250',
            'price'=>'12.99',
            'year_published'=>'1985',
            'author_id'=>'1'
        ]);
        DB::table('books')->insert([
            'title'=>'Black Ice',
            'num_pages'=>'300',
            'price'=>'14.99',
            'year_published'=>'1980',
            'author_id'=>'1'
        ]);
        DB::table('books')->insert([
            'title'=>'Caves of Steel',
            'num_pages'=>'360',
            'price'=>'18.99',
            'year_published'=>'1970',
            'author_id'=>'2'
        ]);
        DB::table('books')->insert([
            'title'=>'Foundation',
            'num_pages'=>'300',
            'price'=>'10.99',
            'year_published'=>'1963',
            'author_id'=>'2'
        ]);
        DB::table('books')->insert([
            'title'=>'The Shining',
            'num_pages'=>'450',
            'price'=>'13.99',
            'year_published'=>'1969',
            'author_id'=>'3'
        ]);
        DB::table('books')->insert([
            'title'=>'IT',
            'num_pages'=>'280',
            'price'=>'20.00',
            'year_published'=>'1960',
            'author_id'=>'3'
        ]);
        DB::table('books')->insert([
            'title'=>'Underworld',
            'num_pages'=>'365',
            'price'=>'15.00',
            'year_published'=>'1987',
            'author_id'=>'4'
        ]);
        DB::table('books')->insert([
            'title'=>'White Noise',
            'num_pages'=>'340',
            'price'=>'20.00',
            'year_published'=>'1990',
            'author_id'=>'4'
        ]);
        DB::table('books')->insert([
            'title'=>'Devil in a Blue Dress',
            'num_pages'=>'372',
            'price'=>'19.50',
            'year_published'=>'1950',
            'author_id'=>'5'
        ]);
        DB::table('books')->insert([
            'title'=>'Fear Itself',
            'num_pages'=>'200',
            'price'=>'11.50',
            'year_published'=>'1930',
            'author_id'=>'5'
        ]);
    }
}

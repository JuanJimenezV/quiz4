@extends('layout')
@section('content')
    <h1>
        Author List
    </h1>

    <table class="table table-bordered table-hover">
        <thead>
        <tr>
            <th>Name</th>
            <th>Country</th>
        </tr>
        </thead>

        <tbody>
        @foreach($authors as $author)
            <tr>
                <td>{{ $author->name }}</td>
                <td>{{ $author->country }}</td>
                <td>
                    <a href="/authors/{{$author->id}}"> view</a>
                </td>
            </tr>
        @endforeach
        </tbody>

    </table>

@endsection

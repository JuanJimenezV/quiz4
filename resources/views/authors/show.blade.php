@extends('layout')
@section('content')


    <!-- Post Content Column -->
    <div class="col-lg-8">

        <!-- Title -->
        <h1 class="mt-4">{{$author->name}}</h1>

        <!-- Author -->
        <p class="lead">
            by
            <a href="#">{{$author->country}}</a>
        </p>
        <p>
            @foreach($author->books as $book)
                <a href="/books/{{ $book->id }}" class="btn
                            btn-primary btn-xs"><i
                        class="fa fa-eye"></i>{{$book->title}}</a>
            @endforeach
        </p>
        <hr>
    </div>
@endsection

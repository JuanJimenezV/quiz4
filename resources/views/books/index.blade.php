@extends('layout')
@section('content')
    <h1>
        Book List
    </h1>

    <table class="table table-bordered table-hover">
        <thead>
        <tr>
            <th>Title</th>
            <th>No. Pages</th>
            <th>Price</th>
            <th>Year Published</th>
            <th>Author</th>
        </tr>
        </thead>

        <tbody>
        @foreach($books as $book)
            <tr>
                <td>{{ $book->title }}</td>
                <td>{{ $book->num_pages }}</td>
                <td>{{ $book->price }}</td>
                <td>{{ $book->year_published }}</td>
                <td>
                    <a href="/authors/{{$book->author_id}}">{{$book->author->name}}</a>
                </td>
            </tr>
        @endforeach
        </tbody>

    </table>

@endsection

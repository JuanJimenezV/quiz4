@extends('layout')
@section('content')


    <!-- Post Content Column -->
    <div class="col-lg-8">

        <!-- Title -->
        <h1 class="mt-4">{{$book->Title}}</h1>
        <p class="lead">
            <label>Number of pages:</label><label>{{$book->num_pages}}</label><br>
        </p>
        <p class="lead">
            <label>Price:</label><label>{{$book->price}}</label><br>
        </p>
        <p class="lead">
            <label>Year Published:</label><label>{{$book->year_published}}</label><br>
        </p>
        <p class="lead">
            <label>Author:</label><a href="/authors/{{$book->author_id}}">{{$book->author->name}}</a><br>
        </p>
        <hr>
    </div>
@endsection

<?php

namespace App\Http\Controllers;
use App\Author;
use Illuminate\Http\Request;

class AuthorController extends Controller
{
    public function index(){
        $authors = Author::all();
        return view('authors.index',compact('authors'));
    }
    public function show($id){
        $author = Author::where('id',$id)->firstOrFail();
        return view('authors.show',compact('author'));
    }
}
